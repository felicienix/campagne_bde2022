#include "../headers/decimal_pi.h"
#include <iostream>

int main()
{
    arma::ivec pi_decimals_input(PI_DECIMALS_PRECISION_MAX, arma::fill::zeros);
    pi_decimals_input.fill(-1);
    char digit = 'x';
    unsigned long int score = 0;
    unsigned int error_counter = 0;
    
    std::cout<<"Entrez les décimals: pi=3.";
    for(unsigned long int i = 0;(digit = getchar()) != '\n';++i)
    {
        
        if(((int)digit < 48 || (int)digit > 57) && (error_counter++) < 1)
        {
            std::cerr<<"invalid character, try again !"<<std::endl;
            return 0;
        }
        else
        {
            pi_decimals_input[i] = (int)digit - '0';
        }
    }


    for(unsigned int i = 0;i<pi_decimals_input.size();++i)
    {
        if(pi_decimals_input[i] != PI_decimals[i])
        {
            break;
        }
        score += 1;

    }

    if(error_counter < 1)
    {   
        std::cout<<"Your score is "<<score<<" !";
    }

    while(!getchar())
    {
        break;
    }

    
    return 0;
}