#include "SFML/Graphics.hpp"
#include "SFML/Main.hpp"
#include <random>
#include <time.h>
#include <iostream>
#include <array>
#define WIDTH 400
#define HEIGHT 200

namespace
{
	std::default_random_engine createRandomEngine()
	{
		auto seed = static_cast<unsigned long>(std::time(nullptr));
		return std::default_random_engine(seed);
	}

	auto RandomEngine = createRandomEngine();
}


static int randomInt(int exclusiveMax)
{
    std::uniform_int_distribution<> distr(0, exclusiveMax - 1);
    return distr(RandomEngine);
}

namespace TwisterColors
{
    //COLOURS
    enum ID
    {
        RED = 0, YELLOW, BLUE, WHITE, MAGENTA, COUNT
    };
}

static sf::Color cTwisterColors[TwisterColors::COUNT] =
    {
        sf::Color::Red, sf::Color::Yellow, sf::Color::Blue,
        sf::Color::White, sf::Color::Magenta
    };

namespace TwisterBodyParts
{
    //BODY PARTS
    enum ID
    {
        RIGHT_HAND = 0, LEFT_HAND, RIGHT_FOOT, LEFT_FOOT, COUNT
    };
}

static sf::IntRect iBodyPartsRects[TwisterBodyParts::COUNT] =
    {
        sf::IntRect(200, 0, 200, 200),  //RIGHT HAND
        sf::IntRect(200, 200, 200, 200), //LEFT HAND
        sf::IntRect(0, 0, 200, 200),    //RIGHT FOOT
        sf::IntRect(0, 200, 200, 200)   //LEFT FOOT
    };

int main()
{
    //create window
    sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "TWISTER APP", sf::Style::Close);

    //used for stable frame rate
    unsigned int frame_rate = 60;
    sf::Clock loop_clock;
    sf::Time frameAcc = sf::Time::Zero, fps = sf::seconds(1.f/(float)frame_rate);

    sf::Texture bodyparts_texture;
    if(!bodyparts_texture.loadFromFile("./textures/body_parts.png"))
    {
        std::cerr<<"error loading body parts from file body_parts.png"<<std::endl;
        return 1;
    }

    //body parts
    std::array<sf::Sprite, TwisterBodyParts::COUNT> bodyParts;
    for(unsigned int i = 0;i<bodyParts.size();++i)
    {
        bodyParts[i] = sf::Sprite(bodyparts_texture, iBodyPartsRects[i]);
    }

    //colors
    std::array<sf::RectangleShape, TwisterColors::COUNT> rect_colors;
    for(unsigned int i = 0;i<rect_colors.size();++i)
    {
        sf::RectangleShape rect{sf::Vector2f(200, 200)};
        rect.setFillColor(cTwisterColors[i]);
        rect.setPosition(WIDTH * 0.5f, 0.f);
        rect_colors[i] = rect;
    }

    unsigned int current_sprite = 0;
    unsigned int current_color = 0;

    //app loop
    while(window.isOpen())
    {
        frameAcc += loop_clock.restart();
        while(frameAcc >= fps)
        {
            frameAcc -= fps;

            //update stuff

        }
        
        sf::Event e;
        while(window.pollEvent(e))
        {
            switch (e.type)
            {
            case sf::Event::Closed:
                window.close();
                break;
            case sf::Event::KeyPressed:
                switch (e.key.code)
                {
                case sf::Keyboard::Space:
                    current_sprite = randomInt(TwisterBodyParts::COUNT);
                    current_color = randomInt(TwisterColors::COUNT);
                    break;
                case sf::Keyboard::Escape:
                    window.close();
                break;
                
                default:
                    break;
                }
            break;
            
            default:
                break;
            }
        }

        window.clear();

        window.draw(bodyParts[current_sprite]);
        window.draw(rect_colors[current_color]);

        window.display();
    }

    return 0;
}